import getHandler from '@/pages/api/getHandler'
import { pinPad } from './util/pinPad'

const handler = getHandler()

handler.get(async (req, res) => {
  const {amount} = req.query
  try {
    res.status(200).json(await pinPad(amount+".00"))
  }
  catch (err) {
    res.status(200).json(4)
  }
})

export default handler
