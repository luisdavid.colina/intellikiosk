import { spawn } from 'child_process'

import { promises as fsPromises } from 'fs'

const files = [
  "C:\\sistemas\\Proceso_OK.txt",
  "C:\\sistemas\\Proceso_NO.txt",
  "C:\\sistemas\\SinconexionNiubiz.txt",
  "C:\\sistemas\\TotalVentaNIUBIZ.txt",
  "C:\\sistemas\\DatosTransaccionVoucherNiubiz.txt",
  "C:\\sistemas\\DatosTransaccionVoucherNiubiz2.txt"
]

export const resetFiles = async () => {
  Promise.all(files.map(file => fsPromises.unlink(file)))
    .then(() => { }).catch(err => { })
}

export const writeAmount = async ( amount: string | string[]) => {
  await fsPromises.writeFile(files[3], amount)
}

export const runPinPad = async () => {
  spawn("cmd.exe", ["/c", "C:\\sistemas\\Luisda.bat"]);
}

export const searchForResponse = async ( time: number) => {
  for (let i = 0; i < time; i++) {
    try {
      await fsPromises.access(files[0])
      return 1
    }
    catch (err) { } try {
      await fsPromises.access(files[1])
      return 2
    } catch (err) { }
    await new Promise((resolve) => setTimeout(resolve, 1000));
  }
  return 0;
};


