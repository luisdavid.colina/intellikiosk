import { resetFiles, writeAmount, runPinPad, searchForResponse } from './pinPadHelpers'

type PinPad = (amount: string | string[]) => Promise<number>;

export const pinPad: PinPad = async (amount) => {
  await resetFiles()
  await writeAmount(amount)
  runPinPad()
  return await searchForResponse(30)
};


