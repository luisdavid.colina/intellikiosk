import getHandler from '@/pages/api/getHandler'
import { pinPad } from './util/niubiz'

const handler = getHandler()

handler.get(async (req, res) => {
  const {amount} = req.query
  try {
    res.status(200).json(await pinPad())
  }
  catch (err) {
    res.status(200).json(4)
  }
})

export default handler
