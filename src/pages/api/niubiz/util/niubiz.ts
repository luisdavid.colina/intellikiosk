import ffi from '@saleae/ffi'

import ref from 'ref'

let shortType = ref.types.short

let dllPath = "C:\\sistemas\\PclUtilities.dll"

let lib = new ffi.Library(dllPath, {
  'dc_init': ['int', [shortType, 'int']]
})

const runDll = async () => {
  try {
    let handle = lib.dc_init(2, 9600)
    console.log(handle)
  } catch (err) {
    console.log(err.message)
  }
}

export const pinPad = async () => {
  await runDll()
};
